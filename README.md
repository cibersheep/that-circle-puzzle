# That Circle Puzzle

Rotate the circles of the puzzle until all match its circle

## Translators
I have added keywords to the desktop file (it hepls to find the app by keyboard from the Dash).
The format is:
`keyword;keyword1;keyword2;`

This doesn't need to be a straight translation. Feel free to add synonyms or suggest modifications. Thanks!

## License

Copyright (C) 2020  Joan CiberSheep

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
