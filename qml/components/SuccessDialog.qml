import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: successDialog
    title: i18n.tr("Congratulations")
    text: i18n.tr("Well done. You have resolved the puzzle!")

    Label {
        text: i18n.tr("Number of movements: %1").arg(movementsPlayed)
    }

    Label {
        text: i18n.tr("Time used: %1").arg(formatTime(timePlayed))
    }

    Button {
        text: i18n.tr("Ok")

        onClicked: {
            PopupUtils.close(successDialog)
            resetCounters()
        }
    }
}

