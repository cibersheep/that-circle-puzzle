/*
 * Copyright (C) 2020  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * circlepuzzle is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.7
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import "components"

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'circlepuzzle.cibersheep'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)
    backgroundColor: "#333"

    readonly property string mainColor: "#f7f7f7"
    property int maxSize: Math.min(width, height)
    property int movementsPlayed: 0
    property int timePlayed: 0


    PageStack {
        id: mainPageStack
        anchors.fill: parent

        Component.onCompleted: mainPageStack.push(mainPageComponent);
    }

    Component {
        id: mainPageComponent

        Page {
            id: mainPage
            width: parent.width
            height: parent.height

            signal checkIfCompleted

            onCheckIfCompleted: {
                root.movementsPlayed = root.movementsPlayed + 1

                if (mainPage.finished()) {
                    timer.running = false
                    PopupUtils.open(successDialog)
                }
            }

            header: PageHeader {
                id: header
                title: timer.running
                    ? formatTime(timePlayed)
                    : i18n.tr('That Circle Puzzle')

                trailingActionBar {
                    actions: [
                        Action {
                            id: actionInfo
                            iconName: "info"
                            shortcut: "Ctrl+i"
                            text: i18n.tr("Information")
                            onTriggered: {
                                Qt.inputMethod.hide();
                                mainPageStack.push(Qt.resolvedUrl("About.qml"));
                            }
                        },

                        Action {
                            id: actionSettings
                            iconName: "media-playlist-shuffle"
                            shortcut: "Ctrl+s"
                            text: i18n.tr("Shuffle")
                            onTriggered: {
                                mainPage.shuffle()
                                timer.running = true
                            }
                        }
                    ]
                }
            }

            Item {
                id: board
                width: maxSize <= units.gu(60)
                        ? units.gu(36)
                        : maxSize <= units.gu(100)
                            ? units.gu(52)
                            : units.gu(90)

                height: width

                anchors {
                    centerIn: parent
                    verticalCenterOffset: header.height / 2
                }

                property real relativeCircleSize: (board.width / 2) * 1.179

                Circle {
                    id: leftTop
                    circleSize: board.relativeCircleSize

                    circleColor: {
                        "center": "turquoise",
                        "colorOrdered": [
                            "turquoise",
                            "purple",
                            "turquoise",
                            "turquoise"
                        ]
                    }

                    visualCircleColor: {
                        "colorOrdered": [
                            "turquoise",
                            "purple",
                            "turquoise",
                            "turquoise"
                        ]
                    }

                    anchors {
                        top: parent.top
                        left: parent.left
                    }

                    onShowSelectionChanged:  if (showSelection) {
                        rightTop.showSelection = false;
                        leftBottom.showSelection = false;
                        rightBottom.showSelection = false;
                    }

                    onColorsUpdated: {
                        rightTop.circleColor.colorOrdered[3] = leftTop.circleColor.colorOrdered[1]
                        leftBottom.circleColor.colorOrdered[0] = leftTop.circleColor.colorOrdered[2]
                        //Send changed signals for the pieces to update
                        rightTop.circleColorChanged()
                        leftBottom.circleColorChanged()

                        mainPage.checkIfCompleted()
                    }

                    z: showSelection ? 10 : 1
                }

                Circle {
                    id: rightTop
                    circleSize: board.relativeCircleSize

                    circleColor: {
                        "center": "purple",
                        "colorOrdered": [
                            "purple",
                            "purple",
                            "orange",
                            "purple" //leftTop.circleColor.colorOrdered[1]
                        ]
                    }

                    visualCircleColor: {
                        "colorOrdered": [
                            "purple",
                            "purple",
                            "orange",
                            "purple" //leftTop.circleColor.colorOrdered[1]
                        ]
                    }

                    anchors {
                        top: parent.top
                        right: parent.right
                    }

                    onShowSelectionChanged:  if (showSelection) {
                        leftTop.showSelection = false;
                        leftBottom.showSelection = false;
                        rightBottom.showSelection = false;
                    }

                    onColorsUpdated: {
                        leftTop.circleColor.colorOrdered[1] = rightTop.circleColor.colorOrdered[3]
                        rightBottom.circleColor.colorOrdered[0] = rightTop.circleColor.colorOrdered[2]
                        //Send changed signals for the pieces to update
                        leftTop.circleColorChanged()
                        rightBottom.circleColorChanged()

                        mainPage.checkIfCompleted()
                    }

                    z: showSelection ? 10 : 2
                }

                Circle {
                    id: leftBottom
                    circleSize: board.relativeCircleSize

                    circleColor: {
                        "center": "green",
                        "colorOrdered": [
                            "turquoise",
                            "green",
                            "green",
                            "green"
                        ]
                    }

                    visualCircleColor: {
                        "colorOrdered": [
                            "turquoise",
                            "green",
                            "green",
                            "green"
                        ]
                    }

                    anchors {
                        bottom: parent.bottom
                        left: parent.left
                    }

                    onShowSelectionChanged:  if (showSelection) {
                        leftTop.showSelection = false;
                        rightTop.showSelection = false;
                        rightBottom.showSelection = false;
                    }

                    onColorsUpdated: {
                        leftTop.circleColor.colorOrdered[2] = leftBottom.circleColor.colorOrdered[0]
                        rightBottom.circleColor.colorOrdered[3] = leftBottom.circleColor.colorOrdered[1]
                        //Send changed signals for the pieces to update
                        leftTop.circleColorChanged()
                        rightBottom.circleColorChanged()

                        mainPage.checkIfCompleted()
                    }

                    z: showSelection ? 10 : 3
                }

                Circle {
                    id: rightBottom
                    circleSize: board.relativeCircleSize

                    circleColor: {
                        "center": "orange",
                        "colorOrdered": [
                            "orange",
                            "orange",
                            "orange",
                            "green"
                        ]
                    }

                    visualCircleColor: {
                        "colorOrdered": [
                            "orange",
                            "orange",
                            "orange",
                            "green"
                        ]
                    }

                    anchors {
                        bottom: parent.bottom
                        right: parent.right
                    }

                    onShowSelectionChanged:  if (showSelection) {
                        leftTop.showSelection = false;
                        rightTop.showSelection = false;
                        leftBottom.showSelection = false;
                    }

                    onColorsUpdated: {
                        rightTop.circleColor.colorOrdered[2] = rightBottom.circleColor.colorOrdered[0]
                        leftBottom.circleColor.colorOrdered[1] = rightBottom.circleColor.colorOrdered[3]
                        //Send changed signals for the pieces to update
                        rightTop.circleColorChanged()
                        leftBottom.circleColorChanged()

                        mainPage.checkIfCompleted()
                    }

                    z: showSelection ? 10 : 4
                }
            }

            Component {
                id: successDialog

                SuccessDialog{}
            }

            function shuffle() {
                var pieces = ["purple", "purple", "purple", "turquoise", "turquoise", "turquoise", "orange", "orange", "orange", "green", "green", "green"]
                var randomPieces = []
                for (var j = 0; j < 12; j++) {
                    var randomElement = Math.floor(Math.random() * pieces.length)
                    randomPieces.push(pieces.splice(randomElement,1))
                }

                for (var k=0; k<4; k++) {
                    leftTop.circleColor.colorOrdered[k] = randomPieces[k]
                }

                rightTop.circleColor.colorOrdered[3] = leftTop.circleColor.colorOrdered[1]
                leftBottom.circleColor.colorOrdered[0] = leftTop.circleColor.colorOrdered[2]

                for (var k=0; k<3; k++) {
                    rightTop.circleColor.colorOrdered[k] = randomPieces[k + 4]
                }

                rightBottom.circleColor.colorOrdered[0] = rightTop.circleColor.colorOrdered[2]

                for (var k=1; k<4; k++) {
                    rightBottom.circleColor.colorOrdered[k] = randomPieces[k + 6]
                }

                leftBottom.circleColor.colorOrdered[1] = rightBottom.circleColor.colorOrdered[3]
                leftBottom.circleColor.colorOrdered[2] = randomPieces[10]
                leftBottom.circleColor.colorOrdered[3] = randomPieces[11]

                rightTop.circleColorChanged()
                rightBottom.circleColorChanged()
                leftTop.circleColorChanged()
                leftBottom.circleColorChanged()
            }

            function finished() {
                function check(string, were, what) {
                    var found = 0

                    for (var i=0; i<were.length; i++) {
                        if (string[were[i]] == what) {
                            found++
                        }
                    }

                    return (found == 3)
                }

                var tourFinishedA = check(leftTop.circleColor.colorOrdered, [0,2,3], leftTop.circleColor.center)
                var tourFinishedB = check(leftTop.circleColor.colorOrdered, [0,1,3], leftTop.circleColor.center)

                var purFinishedA = check(rightTop.circleColor.colorOrdered, [0,1,3], rightTop.circleColor.center)
                var purFinishedB = check(rightTop.circleColor.colorOrdered, [0,1,2], rightTop.circleColor.center)

                var greFinishedA = check(leftBottom.circleColor.colorOrdered, [1,2,3], leftBottom.circleColor.center)
                var greFinishedB = check(leftBottom.circleColor.colorOrdered, [0,2,3], leftBottom.circleColor.center)

                var oraFinishedA = check(rightBottom.circleColor.colorOrdered, [0,1,2], rightBottom.circleColor.center)
                var oraFinishedB = check(rightBottom.circleColor.colorOrdered, [1,2,3], rightBottom.circleColor.center)

                return (tourFinishedA && purFinishedA && greFinishedA && oraFinishedA || tourFinishedB && purFinishedB && greFinishedB && oraFinishedB)
            }

            function resetCounters() {
                movementsPlayed = 0
                timePlayed = 0
            }

            function formatTime(time) {
              var minutes = Math.floor(time / 60);
              var seconds = (time % 60).toFixed(0);
              return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
            }

            Timer {
                id: timer
                running: false
                repeat: true

                onTriggered: root.timePlayed = root.timePlayed + 1
            }
        }
    }
}
